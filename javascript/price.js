
let priceBtn = document.querySelectorAll("#price-btn");
let priceModal = document.querySelector("#price-modal");
let closeBtn = document.querySelector("#close-btn");
priceBtn.forEach((e) => {
    e.addEventListener("click", function () {
        priceModal.classList.remove("d-none");
        priceModal.classList.add("d-block");
    })
})

closeBtn.addEventListener("click", function () {
    priceModal.classList.remove("d-block");
    priceModal.classList.add("d-none");
})



// userRange

let userRange = document.getElementById("userRange");
let usersInput = document.getElementById("usersInput");
let [card1,card2,card3] = document.querySelectorAll(".card");

userRange.addEventListener("input", function () {
    let users = userRange.value;
    usersInput.innerHTML = users;

    if (users > 0 && users <= 10  ) {
        card1.classList.add("card-highlight");
        card2.classList.remove("card-highlight");
        card3.classList.remove("card-highlight");
    }
   
    else if (users > 10 && users <= 20) {
        card1.classList.remove("card-highlight");
        card2.classList.add("card-highlight");
        card3.classList.remove("card-highlight");
    }
  
    else if (users > 20 && users <= 30 ) {
        card1.classList.remove("card-highlight");
        card2.classList.remove("card-highlight");
        card3.classList.add("card-highlight");

    }


});
